import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please Input aee[" + i + "]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        System.out.println("Sum = " + sum);
        double avg = ((double) sum) / arr.length;
        System.out.println("avg = " + avg);

        int MinIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[MinIndex] > arr[i]) {
                MinIndex = i;

            }
        }
        System.out.println("min = " + arr[MinIndex] + " ,index = " + MinIndex);

        int MaxIndex=0 ;
        for(int i=1; i<arr.length; i++){
            if(arr[i]>arr[MaxIndex]){
                MaxIndex=i ;
            }
        }    
        System.out.println("max = " + arr[MaxIndex] + " ,index = " + MaxIndex);    
    }
}
